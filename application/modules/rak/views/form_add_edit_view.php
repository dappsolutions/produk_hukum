<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="row">
 <div class="col-md-12">  
  <!-- Horizontal Form -->
  <div class="box box-info padding-16">
   <!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
   <?php echo strtoupper($title_content) ?>
       </div>
      </div>-->
   <div class="box-header with-border" style="margin-top: 12px;">
    <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'FORM' ?></h3>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <form class="form-horizontal" method="post">
    <div class="box-body">
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Kode Rak</label>

      <div class="col-sm-4">
       <input type="text" class="form-control required" 
              error="Kode Rak" id="kode_rak" 
              placeholder="Kode Rak"
              value="<?php echo isset($kode_rak) ? $kode_rak : '' ?>">
      </div>
     </div>
     
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Rak</label>

      <div class="col-sm-4">
       <input type="text" class="form-control required" 
              error="Rak" id="nama" 
              placeholder="Rak"
              value="<?php echo isset($nama_rak) ? $nama_rak : '' ?>">
      </div>
     </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
     <button type="button" class="btn btn-default" onclick="Rak.back()">Cancel</button>
     <button type="submit" class="btn btn-success pull-right" onclick="Rak.simpan('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-check"></i>&nbsp;Proses</button>
    </div>
    <!-- /.box-footer -->
   </form>
  </div>
  <!-- /.box -->
 </div>
</div>
