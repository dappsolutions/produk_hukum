<?php if (!empty($data_produk)) { ?>
	<?php $no = 1; ?>
	<?php foreach ($data_produk as $value) { ?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo $value['no_arsip'] ?></td>
			<td><?php echo $value['kode_jenis'] ?></td>
			<td><?php echo $value['no_produk'] ?></td>
			<td><?php echo $value['tahun_produk'] ?></td>
			<td><?php echo $value['judul'] ?></td>
			<?php $text_color = $value['status_produk'] == 'AKTIF' ? 'text-success' : 'text-danger' ?>
			<td class="<?php echo $text_color ?>">
				<?php echo $value['status_produk'] ?>
			</td>
		</tr>
	<?php } ?>
<?php } else { ?>
	<tr>
		<td colspan="8" class="text-center">Tidak ada data ditemukan</td>
	</tr>
<?php } ?>
