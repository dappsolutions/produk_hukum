<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body box-block">
				<div class="row">
					<div class="col-md-4">
						<select class="form-control required" id="tahun" error="Tahun" onchange="">
							<option value="">Pilih Tahun</option>
							<?php if (!empty($list_tahun)) { ?>
								<?php foreach ($list_tahun as $value) { ?>
									<?php $selected = '' ?>
									<option tahun="<?php echo $value['tahun'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['tahun'] ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</div>
					<div class='col-md-4'>
						<select class="form-control required" id="jenis" error="Jenis Produk" onchange="">
							<option value="">Pilih Jenis Produk</option>
							<?php if (!empty($list_jenis)) { ?>
								<?php foreach ($list_jenis as $value) { ?>
									<?php $selected = '' ?>
									<option kode="<?php echo $value['kode_jenis'] ?>" kategori="<?php echo $value['jenis'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['kode_jenis'] . '-' . $value['jenis'] ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</div>
					<div class='col-md-4'>
						<button id="tampil" class="btn btn-primary" onclick="LapProduk.tampilkan(this)">Tampilkan</button>
						&nbsp;
						<a class="btn btn-success" download="<?php echo 'Laporan Produk Hukum' ?>.xls" href="#" id="" onclick="return ExcellentExport.excel(this, 'tb_laporan', 'Laporan Produk');">Export</a>
					</div>
				</div>
				<br />
				<br />
				<div class="row">
					<div class="col-md-12">
						<div class='table-responsive' id="data_detail">
							<table class="table table-bordered" id="tb_laporan">
								<thead>
									<tr class="bg-primary-light text-white">
										<th>No</th>
										<th>No Arsip</th>
										<th>Jenis Produk Hukum</th>
										<th>No Produk Hukum</th>
										<th>Tahun</th>
										<th>Judul</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<?php if (!empty($data_produk)) { ?>
										<?php $no = 1; ?>
										<?php foreach ($data_produk as $value) { ?>
											<tr>
												<td><?php echo $no++ ?></td>
												<td><?php echo $value['no_arsip'] ?></td>
												<td><?php echo $value['kode_jenis'] ?></td>
												<td><?php echo $value['no_produk'] ?></td>
												<td><?php echo $value['tahun_produk'] ?></td>
												<td><?php echo $value['judul'] ?></td>
												<?php $text_color = $value['status_produk'] == 'AKTIF' ? 'text-success' : 'text-danger' ?>
												<td class="<?php echo $text_color ?>">
													<?php echo $value['status_produk'] ?>
												</td>
											</tr>
										<?php } ?>
									<?php } else { ?>
										<tr>
											<td colspan="8" class="text-center">Tidak ada data ditemukan</td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
