<?php

class Lapproduk extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'lapproduk';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
      '<link rel="stylesheet" href="' . base_url() . 'assets/admin_lte/bower_components/bootstrap-daterangepicker/daterangepicker.css">',
      '<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/min/moment.min.js"></script>',
      '<script src="' . base_url() . 'assets/admin_lte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/lapproduk.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'produk';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Produk";
  $data['title_content'] = 'Data Produk';

//  echo 'asdasd';die;
  $content = $this->getDataProdukDetail();
	$data['data_produk'] = $content['data'];
	$data['list_tahun'] = $this->getListTahun();
	$data['list_jenis'] = $this->getListJenisProduk();
  echo Modules::run('template', $data);
 }

 public function getListJenisProduk() {
  $data = Modules::run('database/get', array(
              'table' => 'jenis_produk jp',
              'field' => array('jp.*'),
              'where' => "jp.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListTahun() {
  $data = Modules::run('database/get', array(
              'table' => 'tahun t',
              'field' => array('t.*'),
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataProdukDetail($keyword = '') {
  $sql = "select 
	p.* 
	, t.tahun as tahun_produk
	, jp.jenis
	, jp.kode_jenis
	, st.status as status_produk
	from produk p
join tahun t
	on t.id = p.tahun
join jenis_produk jp
	on jp.id = p.jenis_produk
join status st
	on st.id = p.status
where p.deleted = 0
";

  $data = Modules::run('database/get_custom', $sql);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;

  return array(
      'data' => $result,
  );
 }

 public function getDataProdukDetailSearch($jenis, $tahun) {

	if($jenis != '' && $tahun != ''){
		$where = "where p.jenis_produk = '" . $jenis . "' "
		. "and p.tahun = '" . $tahun . "' and p.deleted = 0";
	} else{
		if($jenis != ''){
			$where = "where p.jenis_produk = '" . $jenis . "' and p.deleted = 0";
		}else{
			$where = "where p.tahun = '" . $tahun . "' and p.deleted = 0";
		}
	} 

  $sql = "select 
	p.* 
	, t.tahun as tahun_produk
	, jp.jenis
	, jp.kode_jenis
	, st.status as status_produk
	from produk p
join tahun t
	on t.id = p.tahun
join jenis_produk jp
	on jp.id = p.jenis_produk
join status st
	on st.id = p.status
" . $where;

  $data = Modules::run('database/get_custom', $sql);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;

  return array(
      'data' => $result,
  );
 }

 public function tampilkan() {
  $jenis = $_POST['jenis'];
  $tahun = $_POST['tahun'];

  $data_laporan = $this->getDataProdukDetailSearch($jenis, $tahun);
  // echo '<pre>';
  // print_r($data_laporan);die;
  $data['data_produk'] = $data_laporan['data'];
  echo $this->load->view('table_laporan', $data, true);
 }

}
