<?php

if (!defined('BASEPATH'))
 exit('No direct script access allowed');

class No_generator extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function digit_count($length, $value) {
  while (strlen($value) < $length)
   $value = '0' . $value;
  return $value;
 }

 public function generateNoArsip() {
  $no = 'ARSIP' . date('y') . strtoupper(date('M'));
  $data = Modules::run('database/get', array(
              'table' => 'penomoran_arsip',
              'like' => array(
                  array('nomor', $no)
              ),
              'orderby' => 'id desc'
  ));

  $seq = 1;
  if (!empty($data)) {
   $data = $data->row_array();
   $seq = str_replace($no, '', $data['nomor']);
   $seq = intval($seq) + 1;
  }

  $seq = $this->digit_count(3, $seq);
  $no .= $seq;
  return $no;
 }

}
