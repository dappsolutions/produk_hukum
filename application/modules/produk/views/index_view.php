<div class="row">    
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box padding-16">
     <div class="box-header with-border">
      <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo strtoupper($title) ?></h3>
      <div class="divider"></div>
     </div>
     <!-- /.box-header -->
     <!-- form start -->
     <div class="box-body">
      <div class="row">
       <div class="col-md-12">
        <div class="input-group">
         <input type="text" class="form-control" onkeyup="Produk.search(this, event)" id="keyword" placeholder="Pencarian">
         <span class="input-group-addon"><i class="fa fa-search"></i></span>
        </div>
       </div>          
      </div>
      <div class="divider"></div>
      <br/>
      <?php if (isset($keyword)) { ?>
       <br/>
       <div class="row">
        <div class="col-md-6">
         Cari : <label class="bold"><b><?php echo $keyword ?></b></label>
        </div>
       </div>
      <?php } ?>      
      <div class="row">
       <div class="col-md-12">        
        <div class="table-responsive">
         <table class="table table-bordered" id="tb_content">
          <thead>
           <tr class="bg-primary-light text-white">
            <th>No</th>
            <th>No Arsip</th>
            <th>Jenis Produk Hukum</th>
            <th>No Produk Hukum</th>
            <th>Tahun</th>
            <th>Judul</th>
            <th>Status</th>
            <th>Action</th>
           </tr>
          </thead>
          <tbody>
           <?php if (!empty($content)) { ?>
            <?php $no = $pagination['last_no'] + 1; ?>
            <?php foreach ($content as $value) { ?>
             <tr>
              <td><?php echo $no++ ?></td>
              <td><?php echo $value['no_arsip'] ?></td>
              <td><?php echo $value['kode_jenis'] ?></td>
              <td><?php echo $value['no_produk'] ?></td>
              <td><?php echo $value['tahun_produk'] ?></td>
              <td><?php echo $value['judul'] ?></td>
              <?php $text_color = $value['status_produk'] == 'AKTIF' ? 'text-success' : 'text-danger' ?>
              <td class="<?php echo $text_color ?>">
               <?php echo $value['status_produk'] ?>
              </td>
              <td class="text-center">
               <i class="fa fa-trash grey-text hover" onclick="Produk.delete('<?php echo $value['id'] ?>')"></i>
               &nbsp;
               <i class="fa fa-pencil grey-text  hover" onclick="Produk.ubah('<?php echo $value['id'] ?>')"></i>              
               &nbsp;
               <i class="fa fa-file-text grey-text  hover" onclick="Produk.detail('<?php echo $value['id'] ?>')"></i>
              </td>
             </tr>
            <?php } ?>
           <?php } else { ?>
            <tr>
             <td colspan="8" class="text-center">Tidak ada data ditemukan</td>
            </tr>
           <?php } ?>           
          </tbody>
         </table>
        </div>
       </div>
      </div>
     </div>

     <div class="row">
      <div class="col-md-12 text-right">
       <span>Menampilkan data <?php echo ($no > 0 ? $pagination['last_no'] + 1 : 0) . ' - ' . ($no - 1) ?>
        dari <b><?php echo $pagination['total_rows'] ?></b> data.</span>
      </div>
     </div>

     <div class="box-footer clearfix">
      <ul class="pagination pagination-sm no-margin pull-right">
       <?php echo $pagination['links'] ?>
      </ul>
     </div>
    </div>    

    <a href="#" class="float" onclick="Produk.add()">
     <i class="fa fa-plus my-float fa-lg"></i>
    </a>
   </div>
  </div>       
 </div>
</div>
