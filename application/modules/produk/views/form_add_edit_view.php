<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="row">
 <div class="col-md-12">  
  <!-- Horizontal Form -->
  <div class="box box-info padding-16">
   <!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
   <?php echo strtoupper($title_content) ?>
       </div>
      </div>-->
   <div class="box-header with-border" style="margin-top: 12px;">
    <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'FORM' ?></h3>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <form class="form-horizontal" method="post">
    <div class="box-body">

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Kode Ruang</label>

      <div class="col-sm-7">
       <select class="form-control required" error="Kode Ruang" 
               id="ruang" onchange="Produk.setNomorArsip()">
        <option value="">Pilih Kode Ruang</option>
        <?php if (!empty($list_ruang)) { ?>
         <?php foreach ($list_ruang as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($ruang)) { ?>
           <?php $selected = $ruang == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option kode="<?php echo $value['kode_ruang'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['kode_ruang'] . ' - ' . $value['nama'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>

     <div class="form-group">      
      <label for="" class="col-sm-2 control-label">Kode Lemari</label>     
      <div class="col-sm-7">
       <select class="form-control required" error="Kode Lemari" 
               id="lemari" onchange="Produk.setNomorArsip()">
        <option value="">Pilih Kode Lemari</option>
        <?php if (!empty($list_lemari)) { ?>
         <?php foreach ($list_lemari as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($lemari)) { ?>
           <?php $selected = $lemari == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option kode="<?php echo $value['kode_lemari'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['kode_lemari'] . ' - ' . $value['nama'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>   
     </div> 

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">No Rak</label>
      <div class="col-sm-7">
       <select class="form-control required" id="rak" 
               error="Rak" onchange="Produk.setNomorArsip()">
        <option value="">Pilih Rak</option>
        <?php if (!empty($list_rak)) { ?>
         <?php foreach ($list_rak as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($rak)) { ?>
           <?php $selected = $rak == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option kode="<?php echo $value['kode_rak'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['kode_rak'] . '-' . $value['nama_rak'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>     
     </div>

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">No Produk</label>
      <div class="col-sm-7">
       <input  type="text" class="form-control required" 
               error="No Produk" id="no_produk" 
               placeholder="No Produk"
               onkeyup="Produk.setNomorArsip()"
               value="<?php echo isset($no_produk) ? $no_produk : '' ?>">
      </div>
     </div> 

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">No Arsip</label>
      <div class="col-sm-7">
       <input readonly="" type="text" class="form-control" id="no_arsip" 
              placeholder="No Arsip"
              value="<?php echo isset($no_arsip) ? $no_arsip : '' ?>">
      </div>
     </div>

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Judul</label>
      <div class="col-sm-7">
       <textarea class="form-control required" 
                 error="Judul" id="judul" 
                 placeholder="Judul"><?php echo isset($judul) ? $judul : '' ?></textarea>
      </div>
     </div>

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Kategori Produk</label>
      <div class="col-sm-7">
       <select class="form-control required" id="jenis" 
               error="Jenis Produk" onchange="Produk.setNomorArsip()">
        <option value="">Pilih Jenis Produk</option>
        <?php if (!empty($list_jenis)) { ?>
         <?php foreach ($list_jenis as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($jenis_produk)) { ?>
           <?php $selected = $jenis_produk == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option kode="<?php echo $value['kode_jenis'] ?>" kategori="<?php echo $value['jenis'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['kode_jenis'].'-'.$value['jenis'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>      
     </div>    

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Tahun</label>
      <div class="col-sm-7">
       <select class="form-control required" id="tahun" 
               error="Tahun" onchange="Produk.setNomorArsip()">
        <option value="">Pilih Tahun</option>
        <?php if (!empty($list_tahun)) { ?>
         <?php foreach ($list_tahun as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($tahun)) { ?>
           <?php $selected = $tahun == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option tahun="<?php echo $value['tahun'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['tahun'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Tanggal Penetapan</label>
      <div class="col-sm-7">
       <input type="text" value="<?php echo isset($tgl_penetapan) ? $tgl_penetapan : '' ?>" id="tgl_penetapan" readonly="" class="form-control" />
      </div>
     </div>

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Menimbang</label>
      <div class="col-sm-7">
       <textarea id="menimbang"  class="form-control textarea-input" ><?php echo isset($menimbang) ? $menimbang : '' ?></textarea>
      </div>
     </div>

     <div class="form-group">            
      <label for="" class="col-sm-2 control-label">Mengingat</label>
      <div class="col-sm-7">
       <textarea id="mengingat"  class="form-control textarea-input" ><?php echo isset($mengingat) ? $mengingat : '' ?></textarea>
      </div>
     </div>

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Memperhatikan</label>
      <div class="col-sm-7">
       <textarea id="memperhatikan" class="form-control textarea-input"><?php echo isset($memperhatikan) ? $memperhatikan : '' ?></textarea>
      </div>      
     </div>       

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Menetapkan</label>
      <div class="col-sm-7">
       <textarea id="menetapakan"  class="form-control textarea-input"><?php echo isset($menetapkan) ? $menetapkan : '' ?></textarea>
      </div>
     </div>


     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Status</label>
      <div class="col-sm-7">
       <select class="form-control required" id="status" error="Status">
        <option value="">Pilih Status</option>
        <?php if (!empty($list_status)) { ?>
         <?php foreach ($list_status as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($status)) { ?>
           <?php $selected = $status == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['status'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>

     <?php $hidden = ""; ?>
     <?php $hidden = isset($file) ? 'hidden' : '' ?>
     <div class="form-group <?php echo $hidden ?>" id="file_input">
      <label for="" class="col-sm-2 control-label">File input</label>
      <div class="col-sm-7">
       <input type="file" id="file" class="form-control" onchange="Produk.checkFile(this)">
      </div>            
     </div>

     <?php $hidden = ""; ?>
     <?php $hidden = isset($file) ? '' : 'hidden' ?>
     <div class="form-group <?php echo $hidden ?>" id="detail_file">
      <label for="" class="col-sm-2 control-label">File input</label>
      <div class="col-sm-7">
       <div class="input-group">
        <input disabled type="text" id="file_str" class="form-control" value="<?php echo isset($file) ? $file : '' ?>">
        <span class="input-group-addon">
         <i class="fa fa-file-text-o hover-content" file="<?php echo isset($file) ? $file : '' ?>" 
            onclick="Produk.showLogo(this, event)"></i>         
        </span>
        <span class="input-group-addon">
         <i class="fa fa-close hover-content"
            onclick="Produk.gantiFile(this, event)"></i>
        </span>
       </div>
      </div>  
     </div>     
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
     <button type="button" class="btn btn-default" onclick="Produk.back()">Cancel</button>
     <button type="submit" class="btn btn-success pull-right" onclick="Produk.simpan('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-check"></i>&nbsp;Proses</button>
    </div>
    <!-- /.box-footer -->
   </form>
  </div>
  <!-- /.box -->
 </div>
</div>
