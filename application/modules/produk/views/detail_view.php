<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<input type='hidden' name='' id='id' class='detail-input' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="row">
 <div class="col-md-12">  
  <!-- Horizontal Form -->
  <div class="box box-info padding-16">
   <!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
   <?php echo strtoupper($title_content) ?>
       </div>
      </div>-->
   <div class="box-header with-border" style="margin-top: 12px;">
    <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'DETAIL' ?></h3>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <form class="form-horizontal" method="post">
    <div class="box-body">
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">No Arsip</label>
      <label for="" class="col-sm-4 control-label text-primary"><?php echo $no_arsip ?></label> 

      <label for="" class="col-sm-2 control-label">Ruang</label>
      <label for="" class="col-sm-4 control-label text-primary"><?php echo $kode_ruang . ' - ' . $nama_ruang ?></label> 
     </div>

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Rak</label>
      <label for="" class="col-sm-4 control-label text-primary"><?php echo $kode_rak.' - '.$nama_rak ?></label> 

      <label for="" class="col-sm-2 control-label">Lemari</label>
      <label for="" class="col-sm-4 control-label text-primary"><?php echo $kode_lemari . ' - ' . $nama_lemari ?></label> 
     </div>

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">No Produk</label>
      <label for="" class="col-sm-4 control-label text-primary"><?php echo $no_produk ?></label> 

      <label for="" class="col-sm-2 control-label">Tahun</label>
      <label for="" class="col-sm-4 control-label text-primary"><?php echo $tahun_str ?></label> 
     </div>     

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Tanggal Penetapan</label>
      <label for="" class="col-sm-4 control-label text-primary"><?php echo $tgl_penetapan ?></label> 

      <label for="" class="col-sm-2 control-label">Judul</label>
      <label for="" class="col-sm-4 control-label text-primary"><?php echo $judul ?></label> 
     </div>

     <div class="form-group">
	 <label for="" class="col-sm-2 control-label">Menimbang</label>
	 	<div class="col-sm-10">
       <textarea id="menetapakan" readonly  class="form-control textarea-input"><?php echo isset($menimbang) ? $menimbang : '' ?></textarea>
      </div>
     </div>
	 
	 <div class="form-group">
	 <label for="" class="col-sm-2 control-label">Mengingat</label>
	 	<div class="col-sm-10">
       <textarea id="menetapakan" readonly  class="form-control textarea-input"><?php echo isset($mengingat) ? $mengingat : '' ?></textarea>
      </div>
     </div>
	 
	 <div class="form-group">
	 <label for="" class="col-sm-2 control-label">Memperhatikan</label>
	 	<div class="col-sm-10">
       <textarea id="menetapakan" readonly  class="form-control textarea-input"><?php echo isset($memperhatikan) ? $memperhatikan : '' ?></textarea>
      </div>
     </div>
	 
	 <div class="form-group">
	 <label for="" class="col-sm-2 control-label">Menetapkan</label>
	 	<div class="col-sm-10">
       <textarea id="menetapakan" readonly  class="form-control textarea-input"><?php echo isset($menetapkan) ? $menetapkan : '' ?></textarea>
      </div>
     </div>

     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Status</label>
      <label for="" class="col-sm-4 control-label text-primary"><?php echo $status_produk ?></label> 
      
      <label for="" class="col-sm-2 control-label">Download File</label>
      <div class="col-sm-4">
       <div class="input-group">
        <input disabled type="text" class="form-control" value="<?php echo isset($file) ? $file : '' ?>">
        <span class="input-group-addon"><i class="fa fa-file-text-o hover-content" file="<?php echo isset($file) ? $file : '' ?>" onclick="Produk.showLogo(this, event)"></i></span>
       </div>
      </div> 
     </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
     <button type="button" class="btn btn-default" onclick="Produk.back()">Kembali</button>
    </div>
    <!-- /.box-footer -->
   </form>
  </div>
  <!-- /.box -->
 </div>
</div>
