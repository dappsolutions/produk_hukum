<?php

class Produk extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
 }

 public function getModuleName() {
  return 'produk';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/tinymce/js/tinymce/tinymce.min.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/produk.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'produk';
 }

 public function getRootModule() {
  return "Data";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Produk";
  $data['title_content'] = 'Produk';
  $content = $this->getDataProduk();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataProduk($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.file', $keyword),
       array('p.judul', $keyword),
       array('p.no_produk', $keyword),
       array('jp.jenis', $keyword),
       array('s.status', $keyword),
	   array('t.tahun', $keyword),
	   array('jp.kode_jenis', $keyword),
       array('p.no_arsip', $keyword),
   );
  }

  $where = "p.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "p.deleted = 0";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' p',
                'field' => array('p.*', 'jp.jenis',
                    's.status as status_produk',
                    't.tahun as tahun_produk'),
                'join' => array(
                    array('jenis_produk jp', 'jp.id = p.jenis_produk'),
                    array('status s', 's.id = p.status'),
                    array('tahun t', 't.id = p.tahun'),
                    array('rak r', 'r.id = p.rak', 'left'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' p',
                'field' => array('p.*', 'jp.jenis',
                    's.status as status_produk',
                    't.tahun as tahun_produk'),        
                'join' => array(
                    array('jenis_produk jp', 'jp.id = p.jenis_produk'),
                    array('status s', 's.id = p.status'),
                    array('tahun t', 't.id = p.tahun'),
                    array('rak r', 'r.id = p.rak', 'left'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataProduk($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.file', $keyword),
       array('p.judul', $keyword),
       array('p.no_produk', $keyword),
       array('jp.jenis', $keyword),
       array('jp.kode_jenis', $keyword),
       array('s.status', $keyword),
       array('t.tahun', $keyword),
       array('p.no_arsip', $keyword),
   );
  }

  $where = "p.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "p.deleted = 0";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' p',
                'field' => array('p.*', 'jp.jenis',
                    's.status as status_produk',
                    't.tahun as tahun_produk', 'jp.kode_jenis'),
                'join' => array(
                    array('jenis_produk jp', 'jp.id = p.jenis_produk'),
                    array('status s', 's.id = p.status'),
                    array('tahun t', 't.id = p.tahun'),
                    array('rak r', 'r.id = p.rak', 'left'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' p',
                'field' => array('p.*', 'jp.jenis',
                    's.status as status_produk',
                    't.tahun as tahun_produk', 'jp.kode_jenis'),
                'join' => array(
                    array('jenis_produk jp', 'jp.id = p.jenis_produk'),
                    array('status s', 's.id = p.status'),
                    array('tahun t', 't.id = p.tahun'),
                    array('rak r', 'r.id = p.rak', 'left'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataProduk($keyword)
  );
 }

 public function getDetailDataProduk($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*', 'th.tahun as tahun_str',
                  'st.status as status_produk', 
                  'ra.kode_ruang', 'ra.nama as nama_ruang',
                  'lr.kode_lemari', 
                  'lr.nama as nama_lemari', 'r.kode_rak', 'r.nama_rak'),
              'join' => array(
                  array('tahun th', 'th.id = t.tahun'),
                  array('status st', 'st.id = t.status'),
                  array('ruang ra', 'ra.id = t.ruang', 'left'),
                  array('lemari lr', 'lr.id = t.lemari', 'left'),
                  array('rak r', 'r.id = t.rak', 'left'),
              ),
              'where' => "t.id = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   $data = $data->row_array();
   $result = $data;
  }
  return $result;
 }

 public function getListTahun() {
  $data = Modules::run('database/get', array(
              'table' => 'tahun t',
              'field' => array('t.*'),
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListStatus() {
  $data = Modules::run('database/get', array(
              'table' => 'status s',
              'field' => array('s.*'),
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListJenisProduk() {
  $data = Modules::run('database/get', array(
              'table' => 'jenis_produk jp',
              'field' => array('jp.*'),
              'where' => "jp.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListRuang() {
  $data = Modules::run('database/get', array(
              'table' => 'ruang',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListLemari() {
  $data = Modules::run('database/get', array(
              'table' => 'lemari',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }
 
 public function getListRak() {
  $data = Modules::run('database/get', array(
              'table' => 'rak',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Produk";
  $data['title_content'] = 'Tambah Produk';
  $data['list_tahun'] = $this->getListTahun();
  $data['list_status'] = $this->getListStatus();
  $data['list_jenis'] = $this->getListJenisProduk();
  $data['list_ruang'] = $this->getListRuang();
  $data['list_lemari'] = $this->getListLemari();
  $data['list_rak'] = $this->getListRak();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataProduk($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Produk";
  $data['title_content'] = 'Ubah Produk';
  $data['list_tahun'] = $this->getListTahun();
  $data['list_status'] = $this->getListStatus();
  $data['list_jenis'] = $this->getListJenisProduk();
  $data['list_ruang'] = $this->getListRuang();
  $data['list_lemari'] = $this->getListLemari();
  $data['list_rak'] = $this->getListRak();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataProduk($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Produk";
  $data['title_content'] = "Detail Produk";
  $data['list_tahun'] = $this->getListTahun();
  $data['list_status'] = $this->getListStatus();
  $data['list_jenis'] = $this->getListJenisProduk();
  $data['list_ruang'] = $this->getListRuang();
  $data['list_lemari'] = $this->getListLemari();
  $data['list_rak'] = $this->getListRak();
  echo Modules::run('template', $data);
 }

 public function getPostDataProduk($value) {
//  $data['keterangan'] = $value->keterangan;
  $data['no_produk'] = $value->no_produk;
  $data['judul'] = $value->judul;
  $data['tahun'] = $value->tahun;
  $data['status'] = $value->status;
  $data['jenis_produk'] = $value->jenis;
  $data['lemari'] = $value->lemari;
  $data['rak'] = $value->rak;
  $data['ruang'] = $value->ruang;
  $data['tgl_penetapan'] = $value->tgl_penetapan;
  $data['menimbang'] = $value->menimbang;
  $data['mengingat'] = $value->mengingat;
  $data['memperhatikan'] = $value->memperhatikan;
  $data['menetapkan'] = $value->menetapakan;
  $data['no_arsip'] = $value->no_arsip;
//  $no_arsip = $value->kode_ruang . '/' . $value->kode_lemari . '-'
//          . $value->rak . '/' . $value->kategori . '/' . $value->no_produk
//          . '/' . $value->tahun_str;
//  $data['no_arsip'] = $no_arsip;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $id = $this->input->post('id');
  $is_valid = false;
  $is_save = true;
  $file = $_FILES;
//  echo '<pre>';
//  print_r($data);die;

  $message = "";
  $this->db->trans_begin();
  try {
   $post = $this->getPostDataProduk($data->form);
   if ($id == '') {
    if (!empty($file)) {
     $response_upload = $this->uploadData('file');
     if ($response_upload['is_valid']) {
      $post['file'] = $file['file']['name'];
     } else {
      $is_save = false;
      $message = $response_upload['response'];
     }
    }
    if ($is_save) {
     $id = Modules::run('database/_insert', $this->getTableName(), $post);
    }
   } else {
    //update
    if (!empty($file)) {
     if ($data->form->file_str != $file['file']['name']) {
      $response_upload = $this->uploadData('file');
      if ($response_upload['is_valid']) {
       $post['file'] = $file['file']['name'];
      } else {
       $is_save = false;
       $message = $response_upload['response'];
      }
     }
    }
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   if ($is_save) {
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Produk";
  $data['title_content'] = 'Data Produk';
  $content = $this->getDataProduk($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/search/'.$keyword.'/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/produk/';
  $config['allowed_types'] = 'png|jpg|jpeg|pdf';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);

  $is_valid = false;
  if (!$this->upload->do_upload($name_of_field)) {
   $response = $this->upload->display_errors();
  } else {
   $response = $this->upload->data();
   $is_valid = true;
  }

  return array(
      'is_valid' => $is_valid,
      'response' => $response
  );
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $foto = $this->input->post('foto');
  $data['foto'] = $foto;
//  echo '<pre>';
//  print_r($data);die;
  echo $this->load->view('foto', $data, true);
 }

 public function getDataWebLimit() {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'where' => 't.deleted = 0',
              'limit' => 4,
              'orderby' => 't.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;
  $data_content['data'] = $result;
  $view = $this->load->view('content', $data_content, true);
  echo json_encode(array('total' => count($result), 'view' => $view));
 }

 public function getDataWeb() {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'where' => 't.deleted = 0',
              'orderby' => 't.id desc, t.tanggal desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $month = date('m', strtotime($value['tanggal']));
    $year = date('Y', strtotime($value['tanggal']));
    $value['month'] = Modules::run('helper/getIndoMonhtDate', $month);
    $group_header_date = $value['month'] . ' ' . $year;
    $value['group_date'] = $group_header_date;
    array_push($result, $value);
   }
  }

  $fix_result = array();
  if (!empty($result)) {
   $temp = "";
   foreach ($result as $value) {
    $group_date = $value['group_date'];
    if ($temp != $group_date) {
     $detail = array();
     foreach ($result as $value_child) {
      $group_date_child = $value_child['group_date'];
      if ($group_date_child == $group_date) {
       array_push($detail, $value_child);
      }
     }

     $fix_result[$group_date]['detail'] = $detail;
     $temp = $group_date;
    }
   }
  }

//  echo '<pre>';
//  print_r($fix_result);
//  die;
  $data_content['data'] = $fix_result;
  $view = $this->load->view('content_all_news', $data_content, true);
  echo $view;
 }

 public function setSessionId($id) {
  $this->session->set_userdata(array(
      'news_id' => $id
  ));
 }

 public function getDetailProduk() {
  $id = $this->session->userdata('news_id');
  $data = $this->getDetailDataProduk($id);

  $data['relate'] = $this->getRelateProduk();
  $view = $this->load->view('content_detail_news', $data, true);
  echo json_encode(array('view' => $view));
 }

 public function getRelateProduk() {
  $id = $this->session->userdata('news_id');
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'where' => "t.deleted = 0",
              'orderby' => 't.id desc, t.tanggal desc'
  ));
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

}
