<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="row">
 <div class="col-md-12">  
  <!-- Horizontal Form -->
  <div class="box box-info padding-16">
   <!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
   <?php echo strtoupper($title_content) ?>
       </div>
      </div>-->
   <div class="box-header with-border" style="margin-top: 12px;">
    <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'FORM' ?></h3>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <form class="form-horizontal" method="post">
    <div class="box-body">
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Kode</label>

      <div class="col-sm-4">
       <input type="text" class="form-control required" 
              error="Kode" id="kode" 
              placeholder="Kode"
              value="<?php echo isset($kode_jenis) ? $kode_jenis : '' ?>">
      </div>
     </div>
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Jenis Produk</label>

      <div class="col-sm-4">
       <input type="text" class="form-control required" 
              error="Jenis" id="jenis" 
              placeholder="Jenis"
              value="<?php echo isset($jenis) ? $jenis : '' ?>">
      </div>
     </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
     <button type="button" class="btn btn-default" onclick="Jenis.back()">Cancel</button>
     <button type="submit" class="btn btn-success pull-right" onclick="Jenis.simpan('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-check"></i>&nbsp;Proses</button>
    </div>
    <!-- /.box-footer -->
   </form>
  </div>
  <!-- /.box -->
 </div>
</div>
