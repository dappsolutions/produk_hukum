<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="row">
 <div class="col-md-12">  
  <!-- Horizontal Form -->
  <div class="box box-info padding-16">
   <!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
   <?php echo strtoupper($title_content) ?>
       </div>
      </div>-->
   <div class="box-header with-border" style="margin-top: 12px;">
    <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'FORM' ?></h3>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <form class="form-horizontal" method="post">
    <div class="box-body">
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Nomor</label>

      <div class="col-sm-4">
       <input disabled type="text" value="<?php echo isset($nomor) ? $nomor : '' ?>" id="nomor" class="form-control required" error="Nomor"/>
      </div>
     </div>
     
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Kode Ruang</label>

      <div class="col-sm-4">
       <select disabled class="form-control required" error="Kode Ruang" id="ruang">
        <option value="">Pilih Kode Ruang</option>
        <?php if (!empty($list_ruang)) { ?>
         <?php foreach ($list_ruang as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($ruang)) { ?>
           <?php $selected = $ruang == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['kode_ruang'].' - '.$value['nama'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>
     
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Kode Lemari</label>

      <div class="col-sm-4">
       <select disabled class="form-control required" error="Kode Lemari" id="lemari">
        <option value="">Pilih Kode Lemari</option>
        <?php if (!empty($list_lemari)) { ?>
         <?php foreach ($list_lemari as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($lemari)) { ?>
           <?php $selected = $lemari == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['kode_lemari'].' - '.$value['nama'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>
     
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Kode Produk</label>

      <div class="col-sm-4">
       <select disabled class="form-control required" error="Kode Produk" id="produk">
        <option value="">Pilih Kode Produk</option>
        <?php if (!empty($list_produk)) { ?>
         <?php foreach ($list_produk as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($produk)) { ?>
           <?php $selected = $produk == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['no_produk'].' - '.$value['judul'] ?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>
     
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Nomor File</label>

      <div class="col-sm-4">
       <select disabled class="form-control required" error="Nomor File" id="nomor_file">
        <option value="">Pilih Nomor File</option>
        <?php if (!empty($list_nomor)) { ?>
         <?php foreach ($list_nomor as $value) { ?>
          <?php $selected = '' ?>
          <?php if (isset($nomor_file)) { ?>
           <?php $selected = $nomor_file == $value['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nomor_file']?></option>
         <?php } ?>
        <?php } ?>
       </select>
      </div>
     </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
     <button type="button" class="btn btn-default" onclick="PenomoranArsip.back()">Cancel</button>
    </div>
    <!-- /.box-footer -->
   </form>
  </div>
  <!-- /.box -->
 </div>
</div>
