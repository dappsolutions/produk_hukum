<?php

class Penomoran_arsip extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;
 public $akses;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  $this->akses = $this->session->userdata('hak_akses');
 }

 public function getModuleName() {
  return 'penomoran_arsip';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/penomoran_arsip.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'penomoran_arsip';
 }

 public function getRootModule() {
  return "Data";
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = $this->getRootModule() . " - Arsip";
  $data['title_content'] = 'Arsip';
  $content = $this->getDataArsip();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataArsip($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('pa.nomor', $keyword),
   );
  }

  $where = "pa.deleted = 0";
  if ($this->akses == 'Superadmin') {
   $where = "pa.deleted = 0";
  }

  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' pa',
                'field' => array('pa.*', 'r.kode_ruang', 
                    'l.kode_lemari', 'p.no_produk', 
                    'nf.nomor_file as no_file'),
                'join' => array(
                    array('ruang r', 'pa.ruang = r.id'),
                    array('lemari l', 'pa.lemari = l.id'),
                    array('produk p', 'pa.produk = p.id'),
                    array('nomor_file nf', 'pa.nomor_file = nf.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'where' => $where
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->getTableName() . ' pa',
                'field' => array('pa.*', 'r.kode_ruang', 
                    'l.kode_lemari', 'p.no_produk', 
                    'nf.nomor_file as no_file'),
                'join' => array(
                    array('ruang r', 'pa.ruang = r.id'),
                    array('lemari l', 'pa.lemari = l.id'),
                    array('produk p', 'pa.produk = p.id'),
                    array('nomor_file nf', 'pa.nomor_file = nf.id'),
                ),
                'is_or_like' => true,
                'like' => $like,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  return $total;
 }

 public function getDataArsip($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('pa.nomor', $keyword),
   );
  }

  $where = "pa.deleted = 0";
  if ($this->akses == 'superadmin') {
   $where = "pa.deleted = 0";
  }


  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' pa',
                'field' => array('pa.*', 'r.kode_ruang', 
                    'l.kode_lemari', 'p.no_produk', 
                    'nf.nomor_file as no_file'),
                'join' => array(
                    array('ruang r', 'pa.ruang = r.id'),
                    array('lemari l', 'pa.lemari = l.id'),
                    array('produk p', 'pa.produk = p.id'),
                    array('nomor_file nf', 'pa.nomor_file = nf.id'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'where' => $where
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->getTableName() . ' pa',
                'field' => array('pa.*', 'r.kode_ruang', 
                    'l.kode_lemari', 'p.no_produk', 
                    'nf.nomor_file as no_file'),
                'join' => array(
                    array('ruang r', 'pa.ruang = r.id'),
                    array('lemari l', 'pa.lemari = l.id'),
                    array('produk p', 'pa.produk = p.id'),
                    array('nomor_file nf', 'pa.nomor_file = nf.id'),
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true,
                'where' => $where
    ));
    break;
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataArsip($keyword)
  );
 }

 public function getDetailDataArsip($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'field' => array('t.*'),
              'where' => "t.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  return $data;
 }

 public function getListRuang() {
  $data = Modules::run('database/get', array(
              'table' => 'ruang',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListLemari() {
  $data = Modules::run('database/get', array(
              'table' => 'lemari',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListNomorFile() {
  $data = Modules::run('database/get', array(
              'table' => 'nomor_file',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListProduk() {
  $data = Modules::run('database/get', array(
              'table' => 'produk p',
              'field' => array('p.*'),
              'join' => array(
                  array('penomoran_arsip pa', 'pa.produk = p.id', 'left'),
                  array('status st', 'st.id = p.status')
              ),
              'where' => "p.deleted = 0 and st.status = 'AKTIF'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Arsip";
  $data['title_content'] = 'Tambah Arsip';
  $data['list_ruang'] = $this->getListRuang();
  $data['list_lemari'] = $this->getListLemari();
  $data['list_produk'] = $this->getListProduk();
  $data['list_nomor'] = $this->getListNomorFile();
  $data['nomor'] = Modules::run('no_generator/generateNoArsip');
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataArsip($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Arsip";
  $data['title_content'] = 'Ubah Arsip';
  $data['list_ruang'] = $this->getListRuang();
  $data['list_lemari'] = $this->getListLemari();
  $data['list_produk'] = $this->getListProduk();
  $data['list_nomor'] = $this->getListNomorFile();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataArsip($id);
//  echo '<pre>';
//  print_r($data);die;
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Arsip";
  $data['title_content'] = "Detail Arsip";
  $data['list_ruang'] = $this->getListRuang();
  $data['list_lemari'] = $this->getListLemari();
  $data['list_produk'] = $this->getListProduk();
  $data['list_nomor'] = $this->getListNomorFile();
  echo Modules::run('template', $data);
 }

 public function getPostDataArsip($value) {
  $data['nomor'] = $value->nomor;
  $data['ruang'] = $value->ruang;
  $data['produk'] = $value->produk;
  $data['lemari'] = $value->lemari;
  $data['nomor_file'] = $value->nomor_file;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;


  $this->db->trans_begin();
  try {
   $post = $this->getPostDataArsip($data->form);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Arsip";
  $data['title_content'] = 'Data Arsip';
  $content = $this->getDataArsip($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/search/' . $keyword . '/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/taruna/';
  $config['allowed_types'] = 'png|jpg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
  echo $this->load->view('foto', $data, true);
 }

 public function getDataWeb() {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' t',
              'where' => 't.deleted = 0'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $data_content['data'] = $result;
  echo $this->load->view('content', $data_content, true);
 }

}
