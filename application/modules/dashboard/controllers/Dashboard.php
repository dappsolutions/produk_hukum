<?php

class Dashboard extends MX_Controller {

 public $hak_akses;
 public $upt;

 public function __construct() {
  parent::__construct();
  date_default_timezone_set("Asia/Jakarta");
  $this->hak_akses = $this->session->userdata('hak_akses');
  $this->upt = $this->session->userdata('upt_id');
 }

 public function getModuleName() {
  return 'dashboard';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/jquery_min_latest.js"></script>',
      '<script src="' . base_url() . 'assets/js/moment_min.js"></script>',
      '<script src="' . base_url() . 'assets/js/daterangepicker.js"></script>',
      '<link href="' . base_url() . 'assets/css/daterangepicker.css" type="text/css" rel="stylesheet"/>',
      '<script src="' . base_url() . 'assets/js/controllers/dashboard.js"></script>',
  );

  return $data;
 }

 public function index() {
  $data['view_file'] = 'v_index';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Dashboard";
  $data['title_content'] = 'Dashboard';
  $data['pengajuan'] = array();
  $data['hak_akses'] = $this->session->userdata('hak_akses');
  $data['total_user'] = $this->getTotalUser();
  $data['total_file'] = $this->getTotalFileProduk();
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function getTotalUser() {
  $total = Modules::run('database/count_all', array(
              'table' => 'user',
              'where' => "deleted = 0"
  ));
  return $total;
 }  
 
 public function getTotalFileProduk() {
  $total = Modules::run('database/count_all', array(
              'table' => 'produk',
              'where' => "deleted = 0 and (file != '' or file is null)"
  ));
  return $total;
 }  
}
