<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>

<div class="row">
 <div class="col-md-12">  
  <!-- Horizontal Form -->
  <div class="box box-info padding-16">
   <!--   <div class="box box-solid box-primary">
       <div class="box-header ui-sortable-handle" style="cursor: move;">
   <?php echo strtoupper($title_content) ?>
       </div>
      </div>-->
   <div class="box-header with-border" style="margin-top: 12px;">
    <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'DETAIL' ?></h3>
   </div>
   <!-- /.box-header -->
   <!-- form start -->
   <form class="form-horizontal" method="post">
    <div class="box-body">
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Username</label>
      <div class="col-sm-4">
       <input disabled type="text" class="form-control required" 
              error="Username" id="username" 
              placeholder="Username"
              value="<?php echo isset($username) ? $username : '' ?>">
      </div>
     </div>
     <div class="form-group">
      <label for="" class="col-sm-2 control-label">Password</label>
      <div class="col-sm-4">
       <input disabled type="text" class="form-control required" 
              error="Password" id="password" 
              placeholder="Password"
              value="<?php echo isset($password) ? $password : '' ?>">
      </div>
	 </div>
	 <div class="form-group">
						<label for="" class="col-sm-2 control-label">Hak Akses</label>
						<div class="col-sm-4">
							<select disabled class="form-control required" id="akses" error="Hak Akses" onchange="">
								<option value="">Pilih Hak Akses</option>
								<?php if (!empty($list_akses)) { ?>
									<?php foreach ($list_akses as $value) { ?>
										<?php $selected = '' ?>
										<?php if (isset($priveledge)) { ?>
											<?php $selected = $priveledge == $value['id'] ? 'selected' : '' ?>
										<?php } ?>
										<option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['hak_akses'] ?></option>
									<?php } ?>
								<?php } ?>
							</select>
						</div>
					</div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
     <button type="button" class="btn btn-default" onclick="User.back()">Cancel</button>
    </div>
    <!-- /.box-footer -->
   </form>
  </div>
  <!-- /.box -->
 </div>
</div>
