<div class="row">    
 <div class="col-md-12">
  <div class="row">
   <div class="col-md-12">
    <div class="box padding-16">
     <div class="box-header with-border">
      <h4 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo strtoupper($title) ?></h4                                                                                                                                                                                                                                                           >
      <div class="divider"></div>
     </div>
     <!-- /.box-header -->
     <!-- form start -->
     <div class="box-body">
      <div class="row">
       <div class="col-md-12">
        <div class="input-group">
         <input type="text" class="form-control" onkeyup="Ruang.search(this, event)" id="keyword" placeholder="Pencarian">
         <span class="input-group-addon"><i class="fa fa-search"></i></span>
        </div>
       </div>          
      </div>
      <div class="divider"></div>
      <br/>
      <?php if (isset($keyword)) { ?>
       <br/>
       <div class="row">
        <div class="col-md-6">
         Cari : <label class="bold"><b><?php echo $keyword ?></b></label>
        </div>
       </div>
      <?php } ?>      
      <div class="row">
       <div class="col-md-12">        
        <div class="table-responsive">
         <table class="table table-bordered" id="tb_content">
          <thead>
           <tr class="bg-primary-light text-white">
            <th>No</th>
            <th>Kode Ruang</th>
            <th>Ruang</th>
            <th>Action</th>
           </tr>
          </thead>
          <tbody>
           <?php if (!empty($content)) { ?>
            <?php $no = $pagination['last_no'] + 1; ?>
            <?php foreach ($content as $value) { ?>
             <tr>
              <td><?php echo $no++ ?></td>
              <td><?php echo $value['kode_ruang'] ?></td>
              <td><?php echo $value['nama'] ?></td>
              <td class="text-center">
               <i class="fa fa-trash grey-text hover" onclick="Ruang.delete('<?php echo $value['id'] ?>')"></i>
               &nbsp;
               <i class="fa fa-pencil grey-text  hover" onclick="Ruang.ubah('<?php echo $value['id'] ?>')"></i>              
               &nbsp;
               <i class="fa fa-file-text grey-text  hover" onclick="Ruang.detail('<?php echo $value['id'] ?>')"></i>
              </td>
             </tr>
            <?php } ?>
           <?php } else { ?>
            <tr>
             <td colspan="5" class="text-center">Tidak ada data ditemukan</td>
            </tr>
           <?php } ?>           
          </tbody>
         </table>
        </div>
       </div>
      </div>
     </div>

     <div class="box-footer clearfix">
      <ul class="pagination pagination-sm no-margin pull-right">
       <?php echo $pagination['links'] ?>
      </ul>
     </div>
    </div>    

    <a href="#" class="float" onclick="Ruang.add()">
     <i class="fa fa-plus my-float fa-lg"></i>
    </a>
   </div>
  </div>       
 </div>
</div>