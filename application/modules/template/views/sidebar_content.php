<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="<?php echo base_url() ?>assets/images/images.png" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p><?php echo ucfirst($this->session->userdata('username')) ?></p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- /.search form -->
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">MAIN NAVIGATION</li>
			<li><a href="<?php echo base_url() . 'dashboard' ?>"><i class="fa fa-folder-o"></i> <span>Dashboard</span></a></li>
			<li class="treeview">
				<a href="#">
					<i class="fa fa-folder"></i>
					<span>Data</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo base_url() . 'produk' ?>"><i class="fa fa-file-text-o"></i> Produk</a></li>
					<?php if ($akses == 'superadmin') { ?>
						<li><a href="<?php echo base_url() . 'jenis' ?>"><i class="fa fa-file-text-o"></i> Jenis Produk</a></li>
						<li><a href="<?php echo base_url() . 'ruang' ?>"><i class="fa fa-file-text-o"></i> Ruang</a></li>
						<li><a href="<?php echo base_url() . 'lemari' ?>"><i class="fa fa-file-text-o"></i> Lemari</a></li>
						<li><a href="<?php echo base_url() . 'rak' ?>"><i class="fa fa-file-text-o"></i> Rak</a></li>
						<!--     <li><a href="<?php echo base_url() . 'penomoran_arsip' ?>"><i class="fa fa-file-text-o"></i> Penomoran Arsip</a></li>
     <li><a href="<?php echo base_url() . 'nomor' ?>"><i class="fa fa-file-text-o"></i> Nomor File</a></li>-->
						<li><a href="<?php echo base_url() . 'user' ?>"><i class="fa fa-file-text-o"></i> Pengguna</a></li>
					<?php } ?>
				</ul>
			</li>

			<li class="treeview">
				<a href="#">
					<i class="fa fa-folder"></i>
					<span>Laporan</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?php echo base_url() . 'lapproduk' ?>"><i class="fa fa-file-text-o"></i> Produk</a></li>
				</ul>
			</li>

			<li><a href="<?php echo base_url() . 'login/sign_out' ?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>
