var Login = {
 module: function () {
  return 'login';
 },

 sign_in: function (elm, e) {
  e.preventDefault();

  var username = $('#username').val();
  var password = $('#password').val();

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: {
     username: username,
     password: password
    },
    dataType: 'json',
    async: false,
    url: url.base_url(Login.module()) + 'sign_in',
    error: function () {
//     message.error('.message', 'Login Gagal, Terjadi Error di Server');
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Masuk...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Login Berhasil");
      setTimeout(Login.goto_dashboard(), 1000);
     } else {
//      message.error('.message', 'Username atau Password Tidak Valid');
      toastr.error("Username atau Password Tidak Valid");
     }
     message.closeLoading();
    }
   });
  }
 },

 goto_dashboard: function () {
  var url = "dashboard/dashboard";
  window.location = url;
 },

 goto_pembelian: function () {
  var url = "pembelian_customer";
  window.location = url;
 },
};
