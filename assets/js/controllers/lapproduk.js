var LapProduk = {
	module: function () {
		return 'lapproduk';
	},

	add: function () {
		window.location.href = url.base_url(LapProduk.module()) + "add";
	},

	back: function () {
		window.location.href = url.base_url(LapProduk.module()) + "index";
	},

	search: function (elm, e) {
		if (e.keyCode == 13) {
			var keyWord = $(elm).val();
			if (keyWord != '') {
				window.location.href = url.base_url(LapProduk.module()) + "search" + '/' + keyWord;
			} else {
				window.location.href = url.base_url(LapProduk.module()) + "index";
			}
		}
	},

	getPostData: function () {
		var data = {
			'id': $('#id').val(),
			'nama': $('#nama').val(),
			'no_hp': $('#no_hp').val()
		};

		return data;
	},

	simpan: function (id) {
		var data = LapProduk.getPostData();
		var formData = new FormData();
		formData.append('data', JSON.stringify(data));
		formData.append("id", id);

		if (validation.run()) {
			$.ajax({
				type: 'POST',
				data: formData,
				dataType: 'json',
				processData: false,
				contentType: false,
				async: false,
				url: url.base_url(LapProduk.module()) + "simpan",
				error: function () {
					toastr.error("Gagal");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Simpan...");
				},

				success: function (resp) {
					if (resp.is_valid) {
						toastr.success("Berhasil Disimpan");
						var reload = function () {
							window.location.href = url.base_url(LapProduk.module()) + "detail" + '/' + resp.tipe_rumah;
						};

						setTimeout(reload(), 1000);
					} else {
						toastr.error("Gagal Disimpan");
					}
					message.closeLoading();
				}
			});
		}
	},

	ubah: function (id) {
		window.location.href = url.base_url(LapProduk.module()) + "ubah/" + id;
	},

	detail: function (id) {
		window.location.href = url.base_url(LapProduk.module()) + "detail/" + id;
	},

	setDate: function () {
		$('#tanggal').daterangepicker();
		// $('input#tanggal').datepicker({
		//  dateFormat: 'yy-mm-dd',
		//  todayHighlight: true,
		//  orientation: 'bottom left'
		// });
	},

	tampilkan: function (elm) {
		var jenis = $('#jenis').val();
		var tahun = $('#tahun').val();
		$.ajax({
			type: 'POST',
			data: {
				tahun: tahun,
				jenis: jenis,
			},
			dataType: 'html',
			async: false,
			url: url.base_url(LapProduk.module()) + "tampilkan",
			error: function () {
				toastr.error("Gagal");
				message.closeLoading();
			},

			beforeSend: function () {
				message.loadingProses("Proses Retrieving Data Laporan...");
			},

			success: function (resp) {
				message.closeLoading();

				$('table#tb_laporan').find('tbody').html(resp);
			}
		});
	},

	setSelect2: function () {
		$('select#tahun').select2();
		$('select#jenis').select2();
	},
};

$(function () {
	LapProduk.setSelect2();
	LapProduk.setDate();
});
